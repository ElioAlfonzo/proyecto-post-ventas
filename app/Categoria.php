<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    
    protected $table = "categorias";

    protected $fillable = ['id','cate_nombre', 'cate_delete', 'cate_active'];

    public $timestamps = false;
}
