<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categoria;

class CategoriaController extends Controller
{
    //
    //Crear Categoria
    public function add(Request $request){

      $name = $request->input("name");

      Categoria::insert([
        'cate_nombre' => $name,
        'cate_active' => 1,
        'cate_delete' => 0
      ]);

      $response['message'] = "Guardo exitosamente $name";
      $response['success'] = true;
      return $response;

    }

    // Load data Categoria
    public function list(){

      $data = Categoria::where("cate_delete",0)
      ->get();
      return $data;
    }

    // available disponible available cate_delete = 1
    public function avalaible(){
      $data = Categoria::where("cate_delete",0)->where("cate_active",1)->get();
      return $data;
    }

    //update status 
    public function change_status(Request $request){

      // id de categoria
      $idcat = $request->input("idcat");
      // el estado de categoria
      $status = $request->input("status");

      Categoria::where("id",$idcat)->update([
        'cate_active' => $status
      ]);

      $response['message'] = "Actualizo exitosamente";
      $response['success'] = true;
      return $response;

    }

}