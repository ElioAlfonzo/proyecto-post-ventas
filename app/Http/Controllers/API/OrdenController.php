<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Orden;
use App\Pedido;

use Carbon\Carbon;

class OrdenController extends Controller
{
    public function create_order(Request $request){

        // obetner un array de pedidos
        $pedido = $request->input("pedidos");

        $orderdata['ord_estado'] = 1; // 1 es igual a pendiente
        $orderdata['ord_mesa']   = $request->input("mesa");
        $orderdata['ord_valor'] = $request->input("total");
        // guardo los datos de la orden
        $order = Orden::create($orderdata);  

        foreach (json_decode($pedido) as $item){

          $itemped['ped_producto'] = $item->id;
          // id de orden del pedido
          $itemped['ped_orden']   = $order->id;
          // cantidad
          $itemped['ped_cantidad'] = $item->cant;
          // precio
          $itemped['ped_valor'] = $item->precio;
          // guarda en base de datos
          Pedido::create($itemped);

        }

        $response['success'] = true;
        $response['message'] = "Guardo exitosamente";
    }

    public function list_order(){
      

      $fecha_hoy = Carbon::now()->format("Y-m-d");

      $orden = Orden::whereDate("ord_fecha",$fecha_hoy)->get();

      foreach ($orden as $value){
        //Adicionar una nueva variable llamada pedidos con los detalles de la orden
        $value['pedidos'] = Pedido::join('productos','productos.id', '=', 'pedidos.ped_producto')
        ->where('ped_orden', $value->ord_id)->get();
        
      }

      return $orden;

    }
}
