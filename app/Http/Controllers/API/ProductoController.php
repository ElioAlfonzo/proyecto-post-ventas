<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
//importar modelo
use App\Productos;
use Log;

class ProductoController extends Controller
{

    public function create(Request $request){

      $input['prod_name'] = $request->input('name'); 
      $input['prod_description'] = $request->input('description'); 
      $input['prod_categoria'] = $request->input('categorie'); 
      $input['prod_price'] = $request->input('price'); 
      
      $input['prod_visible'] = 1;
      $input['prod_delete'] = 0;
      $input['prod_image'] = null;

      if ($request->file('prod_image')==null)
      {
        $input['prod_image'] = null;
      }
      else
      {
        $image = $request->file('prod_image');
        // almacena y captura el nombre del archivo
        $input['prod_image'] =  $image->store('producto','public');
      }

      $data = Productos::insert($input);

      $response['message'] = "Guardo exitosamente ";
      $response['success'] = true;
      return $response;

    }

    public function list(){
      
      $data = Productos::where("prod_delete",0)
      ->join('categorias', 'categorias.id', '=', 'productos.prod_categoria')
      ->select('productos.id as idpro','categorias.cate_nombre','productos.prod_name','productos.prod_price','productos.prod_delete',
      'productos.prod_visible')
      ->orderby('productos.id')->get();

      return $data;
    }

    public function change_status(Request $request){
      //id del product
      $idprod = $request->input("idprod");
      //estado del producto
      $status = $request->input("status");

      // Log::info("Actualizando status $idprod => $status");

      Productos::where("id",$idprod)->update([
        'prod_visible' => $status
      ]);

      $response['message'] = "Actualizo exitosamente";
      $response['success'] = true;
      return $response;
    } 

    public function visible(){

      $data = Productos::where("prod_delete",0)
      ->where("prod_visible",1)
      ->join('categorias', 'categorias.id', '=', 'productos.prod_categoria')
      ->select('productos.id as idpro','categorias.cate_nombre','productos.prod_name','productos.prod_price','productos.prod_delete',
      'productos.prod_visible','productos.prod_image','productos.prod_description','productos.prod_categoria')
      ->where("categorias.cate_active",1)
      ->get();

      foreach ($data as  $value) {
        $value['prod_image'] = asset('storage/'.$value->prod_image);
      }

      return $data;

    }
}